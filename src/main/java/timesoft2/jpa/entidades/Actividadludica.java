/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timesoft2.jpa.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author PC
 */
@Entity
@Table(name = "ACTIVIDADLUDICA", catalog = "", schema = "TIMES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Actividadludica.findAll", query = "SELECT a FROM Actividadludica a")
    , @NamedQuery(name = "Actividadludica.findByPkActividadludica", query = "SELECT a FROM Actividadludica a WHERE a.pkActividadludica = :pkActividadludica")
    , @NamedQuery(name = "Actividadludica.findByNombre", query = "SELECT a FROM Actividadludica a WHERE a.nombre = :nombre")})
public class Actividadludica implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PK_ACTIVIDADLUDICA", nullable = false, precision = 0, scale = -127)
    private BigDecimal pkActividadludica;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "NOMBRE", nullable = false, length = 200)
    private String nombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkActividadludica")
    private List<Actividadludhorario> actividadludhorarioList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkActividadludica")
    private List<Alinstructor> alinstructorList;
    @OneToMany(mappedBy = "fkActividadludica")
    private List<Asistencia> asistenciaList;
    @JoinColumn(name = "FK_LUDICA", referencedColumnName = "PK_LUDICA")
    @ManyToOne
    private Ludica fkLudica;

    public Actividadludica() {
    }

    public Actividadludica(BigDecimal pkActividadludica) {
        this.pkActividadludica = pkActividadludica;
    }

    public Actividadludica(BigDecimal pkActividadludica, String nombre) {
        this.pkActividadludica = pkActividadludica;
        this.nombre = nombre;
    }

    public BigDecimal getPkActividadludica() {
        return pkActividadludica;
    }

    public void setPkActividadludica(BigDecimal pkActividadludica) {
        this.pkActividadludica = pkActividadludica;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlTransient
    public List<Actividadludhorario> getActividadludhorarioList() {
        return actividadludhorarioList;
    }

    public void setActividadludhorarioList(List<Actividadludhorario> actividadludhorarioList) {
        this.actividadludhorarioList = actividadludhorarioList;
    }

    @XmlTransient
    public List<Alinstructor> getAlinstructorList() {
        return alinstructorList;
    }

    public void setAlinstructorList(List<Alinstructor> alinstructorList) {
        this.alinstructorList = alinstructorList;
    }

    @XmlTransient
    public List<Asistencia> getAsistenciaList() {
        return asistenciaList;
    }

    public void setAsistenciaList(List<Asistencia> asistenciaList) {
        this.asistenciaList = asistenciaList;
    }

    public Ludica getFkLudica() {
        return fkLudica;
    }

    public void setFkLudica(Ludica fkLudica) {
        this.fkLudica = fkLudica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkActividadludica != null ? pkActividadludica.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Actividadludica)) {
            return false;
        }
        Actividadludica other = (Actividadludica) object;
        if ((this.pkActividadludica == null && other.pkActividadludica != null) || (this.pkActividadludica != null && !this.pkActividadludica.equals(other.pkActividadludica))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "timesoft2.jpa.entidades.Actividadludica[ pkActividadludica=" + pkActividadludica + " ]";
    }
    
}
