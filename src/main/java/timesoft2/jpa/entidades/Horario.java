/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timesoft2.jpa.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author PC
 */
@Entity
@Table(name = "HORARIO", catalog = "", schema = "TIMES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Horario.findAll", query = "SELECT h FROM Horario h")
    , @NamedQuery(name = "Horario.findByPkHorario", query = "SELECT h FROM Horario h WHERE h.pkHorario = :pkHorario")
    , @NamedQuery(name = "Horario.findByHorainicio", query = "SELECT h FROM Horario h WHERE h.horainicio = :horainicio")
    , @NamedQuery(name = "Horario.findByHorafin", query = "SELECT h FROM Horario h WHERE h.horafin = :horafin")
    , @NamedQuery(name = "Horario.findByDia", query = "SELECT h FROM Horario h WHERE h.dia = :dia")})
public class Horario implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PK_HORARIO", nullable = false, precision = 0, scale = -127)
    private BigDecimal pkHorario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "HORAINICIO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date horainicio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "HORAFIN", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date horafin;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "DIA", nullable = false, length = 50)
    private String dia;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkHorario")
    private List<Actividadludhorario> actividadludhorarioList;
    @OneToMany(mappedBy = "fkHorario")
    private List<Ludica> ludicaList;
    @JoinColumn(name = "FK_LUGAR", referencedColumnName = "PK_LUGAR", nullable = false)
    @ManyToOne(optional = false)
    private Lugar fkLugar;

    public Horario() {
    }

    public Horario(BigDecimal pkHorario) {
        this.pkHorario = pkHorario;
    }

    public Horario(BigDecimal pkHorario, Date horainicio, Date horafin, String dia) {
        this.pkHorario = pkHorario;
        this.horainicio = horainicio;
        this.horafin = horafin;
        this.dia = dia;
    }

    public BigDecimal getPkHorario() {
        return pkHorario;
    }

    public void setPkHorario(BigDecimal pkHorario) {
        this.pkHorario = pkHorario;
    }

    public Date getHorainicio() {
        return horainicio;
    }

    public void setHorainicio(Date horainicio) {
        this.horainicio = horainicio;
    }

    public Date getHorafin() {
        return horafin;
    }

    public void setHorafin(Date horafin) {
        this.horafin = horafin;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    @XmlTransient
    public List<Actividadludhorario> getActividadludhorarioList() {
        return actividadludhorarioList;
    }

    public void setActividadludhorarioList(List<Actividadludhorario> actividadludhorarioList) {
        this.actividadludhorarioList = actividadludhorarioList;
    }

    @XmlTransient
    public List<Ludica> getLudicaList() {
        return ludicaList;
    }

    public void setLudicaList(List<Ludica> ludicaList) {
        this.ludicaList = ludicaList;
    }

    public Lugar getFkLugar() {
        return fkLugar;
    }

    public void setFkLugar(Lugar fkLugar) {
        this.fkLugar = fkLugar;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkHorario != null ? pkHorario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Horario)) {
            return false;
        }
        Horario other = (Horario) object;
        if ((this.pkHorario == null && other.pkHorario != null) || (this.pkHorario != null && !this.pkHorario.equals(other.pkHorario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "timesoft2.jpa.entidades.Horario[ pkHorario=" + pkHorario + " ]";
    }
    
}
