/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timesoft2.jpa.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author PC
 */
@Entity
@Table(name = "FICHA", catalog = "", schema = "TIMES", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"CODIGO"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ficha.findAll", query = "SELECT f FROM Ficha f")
    , @NamedQuery(name = "Ficha.findByPkFicha", query = "SELECT f FROM Ficha f WHERE f.pkFicha = :pkFicha")
    , @NamedQuery(name = "Ficha.findByCodigo", query = "SELECT f FROM Ficha f WHERE f.codigo = :codigo")})
public class Ficha implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PK_FICHA", nullable = false, precision = 0, scale = -127)
    private BigDecimal pkFicha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODIGO", nullable = false)
    private BigInteger codigo;
    @OneToMany(mappedBy = "fkFicha")
    private List<Aprendizficha> aprendizfichaList;
    @JoinColumn(name = "FK_PROGRAMAFORMACION", referencedColumnName = "PK_PROGRAMAFORMACION")
    @ManyToOne
    private Programaformacion fkProgramaformacion;

    public Ficha() {
    }

    public Ficha(BigDecimal pkFicha) {
        this.pkFicha = pkFicha;
    }

    public Ficha(BigDecimal pkFicha, BigInteger codigo) {
        this.pkFicha = pkFicha;
        this.codigo = codigo;
    }

    public BigDecimal getPkFicha() {
        return pkFicha;
    }

    public void setPkFicha(BigDecimal pkFicha) {
        this.pkFicha = pkFicha;
    }

    public BigInteger getCodigo() {
        return codigo;
    }

    public void setCodigo(BigInteger codigo) {
        this.codigo = codigo;
    }

    @XmlTransient
    public List<Aprendizficha> getAprendizfichaList() {
        return aprendizfichaList;
    }

    public void setAprendizfichaList(List<Aprendizficha> aprendizfichaList) {
        this.aprendizfichaList = aprendizfichaList;
    }

    public Programaformacion getFkProgramaformacion() {
        return fkProgramaformacion;
    }

    public void setFkProgramaformacion(Programaformacion fkProgramaformacion) {
        this.fkProgramaformacion = fkProgramaformacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkFicha != null ? pkFicha.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ficha)) {
            return false;
        }
        Ficha other = (Ficha) object;
        if ((this.pkFicha == null && other.pkFicha != null) || (this.pkFicha != null && !this.pkFicha.equals(other.pkFicha))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "timesoft2.jpa.entidades.Ficha[ pkFicha=" + pkFicha + " ]";
    }
    
}
