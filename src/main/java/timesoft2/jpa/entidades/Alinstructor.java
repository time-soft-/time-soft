/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timesoft2.jpa.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PC
 */
@Entity
@Table(name = "ALINSTRUCTOR", catalog = "", schema = "TIMES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Alinstructor.findAll", query = "SELECT a FROM Alinstructor a")
    , @NamedQuery(name = "Alinstructor.findByPkAlinstructor", query = "SELECT a FROM Alinstructor a WHERE a.pkAlinstructor = :pkAlinstructor")})
public class Alinstructor implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PK_ALINSTRUCTOR", nullable = false, precision = 0, scale = -127)
    private BigDecimal pkAlinstructor;
    @JoinColumn(name = "FK_ACTIVIDADLUDICA", referencedColumnName = "PK_ACTIVIDADLUDICA", nullable = false)
    @ManyToOne(optional = false)
    private Actividadludica fkActividadludica;
    @JoinColumn(name = "FK_INSTRUCTOR", referencedColumnName = "PK_INSTRUCTOR", nullable = false)
    @ManyToOne(optional = false)
    private Instructor fkInstructor;

    public Alinstructor() {
    }

    public Alinstructor(BigDecimal pkAlinstructor) {
        this.pkAlinstructor = pkAlinstructor;
    }

    public BigDecimal getPkAlinstructor() {
        return pkAlinstructor;
    }

    public void setPkAlinstructor(BigDecimal pkAlinstructor) {
        this.pkAlinstructor = pkAlinstructor;
    }

    public Actividadludica getFkActividadludica() {
        return fkActividadludica;
    }

    public void setFkActividadludica(Actividadludica fkActividadludica) {
        this.fkActividadludica = fkActividadludica;
    }

    public Instructor getFkInstructor() {
        return fkInstructor;
    }

    public void setFkInstructor(Instructor fkInstructor) {
        this.fkInstructor = fkInstructor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkAlinstructor != null ? pkAlinstructor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Alinstructor)) {
            return false;
        }
        Alinstructor other = (Alinstructor) object;
        if ((this.pkAlinstructor == null && other.pkAlinstructor != null) || (this.pkAlinstructor != null && !this.pkAlinstructor.equals(other.pkAlinstructor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "timesoft2.jpa.entidades.Alinstructor[ pkAlinstructor=" + pkAlinstructor + " ]";
    }
    
}
