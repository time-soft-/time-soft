/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timesoft2.jpa.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author PC
 */
@Entity
@Table(name = "INSTRUCTOR", catalog = "", schema = "TIMES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Instructor.findAll", query = "SELECT i FROM Instructor i")
    , @NamedQuery(name = "Instructor.findByPkInstructor", query = "SELECT i FROM Instructor i WHERE i.pkInstructor = :pkInstructor")
    , @NamedQuery(name = "Instructor.findByNumeroidentificacion", query = "SELECT i FROM Instructor i WHERE i.numeroidentificacion = :numeroidentificacion")
    , @NamedQuery(name = "Instructor.findByNombre", query = "SELECT i FROM Instructor i WHERE i.nombre = :nombre")})
public class Instructor implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PK_INSTRUCTOR", nullable = false, precision = 0, scale = -127)
    private BigDecimal pkInstructor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUMEROIDENTIFICACION", nullable = false)
    private BigInteger numeroidentificacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "NOMBRE", nullable = false, length = 200)
    private String nombre;
    @JoinColumn(name = "FK_PERSONA", referencedColumnName = "PK_PERSONA")
    @ManyToOne
    private Persona fkPersona;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkInstructor")
    private List<Alinstructor> alinstructorList;

    public Instructor() {
    }

    public Instructor(BigDecimal pkInstructor) {
        this.pkInstructor = pkInstructor;
    }

    public Instructor(BigDecimal pkInstructor, BigInteger numeroidentificacion, String nombre) {
        this.pkInstructor = pkInstructor;
        this.numeroidentificacion = numeroidentificacion;
        this.nombre = nombre;
    }

    public BigDecimal getPkInstructor() {
        return pkInstructor;
    }

    public void setPkInstructor(BigDecimal pkInstructor) {
        this.pkInstructor = pkInstructor;
    }

    public BigInteger getNumeroidentificacion() {
        return numeroidentificacion;
    }

    public void setNumeroidentificacion(BigInteger numeroidentificacion) {
        this.numeroidentificacion = numeroidentificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Persona getFkPersona() {
        return fkPersona;
    }

    public void setFkPersona(Persona fkPersona) {
        this.fkPersona = fkPersona;
    }

    @XmlTransient
    public List<Alinstructor> getAlinstructorList() {
        return alinstructorList;
    }

    public void setAlinstructorList(List<Alinstructor> alinstructorList) {
        this.alinstructorList = alinstructorList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkInstructor != null ? pkInstructor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Instructor)) {
            return false;
        }
        Instructor other = (Instructor) object;
        if ((this.pkInstructor == null && other.pkInstructor != null) || (this.pkInstructor != null && !this.pkInstructor.equals(other.pkInstructor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "timesoft2.jpa.entidades.Instructor[ pkInstructor=" + pkInstructor + " ]";
    }
    
}
