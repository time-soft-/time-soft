/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timesoft2.jpa.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author PC
 */
@Entity
@Table(name = "LUDICA", catalog = "", schema = "TIMES", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"CODIGO"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ludica.findAll", query = "SELECT l FROM Ludica l")
    , @NamedQuery(name = "Ludica.findByPkLudica", query = "SELECT l FROM Ludica l WHERE l.pkLudica = :pkLudica")
    , @NamedQuery(name = "Ludica.findByCodigo", query = "SELECT l FROM Ludica l WHERE l.codigo = :codigo")
    , @NamedQuery(name = "Ludica.findByNombre", query = "SELECT l FROM Ludica l WHERE l.nombre = :nombre")})
public class Ludica implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PK_LUDICA", nullable = false, precision = 0, scale = -127)
    private BigDecimal pkLudica;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODIGO", nullable = false)
    private BigInteger codigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NOMBRE", nullable = false)
    private BigInteger nombre;
    @OneToMany(mappedBy = "fkLudica")
    private List<Estadoludica> estadoludicaList;
    @JoinColumn(name = "FK_HORARIO", referencedColumnName = "PK_HORARIO")
    @ManyToOne
    private Horario fkHorario;
    @OneToMany(mappedBy = "fkLudica")
    private List<Actividadludica> actividadludicaList;

    public Ludica() {
    }

    public Ludica(BigDecimal pkLudica) {
        this.pkLudica = pkLudica;
    }

    public Ludica(BigDecimal pkLudica, BigInteger codigo, BigInteger nombre) {
        this.pkLudica = pkLudica;
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public BigDecimal getPkLudica() {
        return pkLudica;
    }

    public void setPkLudica(BigDecimal pkLudica) {
        this.pkLudica = pkLudica;
    }

    public BigInteger getCodigo() {
        return codigo;
    }

    public void setCodigo(BigInteger codigo) {
        this.codigo = codigo;
    }

    public BigInteger getNombre() {
        return nombre;
    }

    public void setNombre(BigInteger nombre) {
        this.nombre = nombre;
    }

    @XmlTransient
    public List<Estadoludica> getEstadoludicaList() {
        return estadoludicaList;
    }

    public void setEstadoludicaList(List<Estadoludica> estadoludicaList) {
        this.estadoludicaList = estadoludicaList;
    }

    public Horario getFkHorario() {
        return fkHorario;
    }

    public void setFkHorario(Horario fkHorario) {
        this.fkHorario = fkHorario;
    }

    @XmlTransient
    public List<Actividadludica> getActividadludicaList() {
        return actividadludicaList;
    }

    public void setActividadludicaList(List<Actividadludica> actividadludicaList) {
        this.actividadludicaList = actividadludicaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkLudica != null ? pkLudica.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ludica)) {
            return false;
        }
        Ludica other = (Ludica) object;
        if ((this.pkLudica == null && other.pkLudica != null) || (this.pkLudica != null && !this.pkLudica.equals(other.pkLudica))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "timesoft2.jpa.entidades.Ludica[ pkLudica=" + pkLudica + " ]";
    }
    
}
