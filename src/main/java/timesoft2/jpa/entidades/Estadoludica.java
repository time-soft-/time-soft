/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timesoft2.jpa.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PC
 */
@Entity
@Table(name = "ESTADOLUDICA", catalog = "", schema = "TIMES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Estadoludica.findAll", query = "SELECT e FROM Estadoludica e")
    , @NamedQuery(name = "Estadoludica.findByPkEstodoludica", query = "SELECT e FROM Estadoludica e WHERE e.pkEstodoludica = :pkEstodoludica")
    , @NamedQuery(name = "Estadoludica.findByFechamodificacion", query = "SELECT e FROM Estadoludica e WHERE e.fechamodificacion = :fechamodificacion")})
public class Estadoludica implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PK_ESTODOLUDICA", nullable = false, precision = 0, scale = -127)
    private BigDecimal pkEstodoludica;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHAMODIFICACION", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechamodificacion;
    @JoinColumn(name = "FK_ESTADO", referencedColumnName = "PK_ESTADO")
    @ManyToOne
    private Estado fkEstado;
    @JoinColumn(name = "FK_EVENTO", referencedColumnName = "PK_EVENTO")
    @ManyToOne
    private Evento fkEvento;
    @JoinColumn(name = "FK_LUDICA", referencedColumnName = "PK_LUDICA")
    @ManyToOne
    private Ludica fkLudica;

    public Estadoludica() {
    }

    public Estadoludica(BigDecimal pkEstodoludica) {
        this.pkEstodoludica = pkEstodoludica;
    }

    public Estadoludica(BigDecimal pkEstodoludica, Date fechamodificacion) {
        this.pkEstodoludica = pkEstodoludica;
        this.fechamodificacion = fechamodificacion;
    }

    public BigDecimal getPkEstodoludica() {
        return pkEstodoludica;
    }

    public void setPkEstodoludica(BigDecimal pkEstodoludica) {
        this.pkEstodoludica = pkEstodoludica;
    }

    public Date getFechamodificacion() {
        return fechamodificacion;
    }

    public void setFechamodificacion(Date fechamodificacion) {
        this.fechamodificacion = fechamodificacion;
    }

    public Estado getFkEstado() {
        return fkEstado;
    }

    public void setFkEstado(Estado fkEstado) {
        this.fkEstado = fkEstado;
    }

    public Evento getFkEvento() {
        return fkEvento;
    }

    public void setFkEvento(Evento fkEvento) {
        this.fkEvento = fkEvento;
    }

    public Ludica getFkLudica() {
        return fkLudica;
    }

    public void setFkLudica(Ludica fkLudica) {
        this.fkLudica = fkLudica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkEstodoludica != null ? pkEstodoludica.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Estadoludica)) {
            return false;
        }
        Estadoludica other = (Estadoludica) object;
        if ((this.pkEstodoludica == null && other.pkEstodoludica != null) || (this.pkEstodoludica != null && !this.pkEstodoludica.equals(other.pkEstodoludica))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "timesoft2.jpa.entidades.Estadoludica[ pkEstodoludica=" + pkEstodoludica + " ]";
    }
    
}
