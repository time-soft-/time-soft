/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timesoft2.jpa.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PC
 */
@Entity
@Table(name = "ASISTENCIA", catalog = "", schema = "TIMES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Asistencia.findAll", query = "SELECT a FROM Asistencia a")
    , @NamedQuery(name = "Asistencia.findByPkAsistencia", query = "SELECT a FROM Asistencia a WHERE a.pkAsistencia = :pkAsistencia")
    , @NamedQuery(name = "Asistencia.findByCantidadhoras", query = "SELECT a FROM Asistencia a WHERE a.cantidadhoras = :cantidadhoras")
    , @NamedQuery(name = "Asistencia.findByFechainicio", query = "SELECT a FROM Asistencia a WHERE a.fechainicio = :fechainicio")
    , @NamedQuery(name = "Asistencia.findByHorasasistidas", query = "SELECT a FROM Asistencia a WHERE a.horasasistidas = :horasasistidas")})
public class Asistencia implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PK_ASISTENCIA", nullable = false, precision = 0, scale = -127)
    private BigDecimal pkAsistencia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CANTIDADHORAS", nullable = false)
    private BigInteger cantidadhoras;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHAINICIO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechainicio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "HORASASISTIDAS", nullable = false)
    private BigInteger horasasistidas;
    @JoinColumn(name = "FK_ACTIVIDADLUDICA", referencedColumnName = "PK_ACTIVIDADLUDICA")
    @ManyToOne
    private Actividadludica fkActividadludica;
    @JoinColumn(name = "FK_APRENDIZ", referencedColumnName = "PK_APRENDIZ", nullable = false)
    @ManyToOne(optional = false)
    private Aprendiz fkAprendiz;
    @JoinColumn(name = "FK_EVENTO", referencedColumnName = "PK_EVENTO")
    @ManyToOne
    private Evento fkEvento;

    public Asistencia() {
    }

    public Asistencia(BigDecimal pkAsistencia) {
        this.pkAsistencia = pkAsistencia;
    }

    public Asistencia(BigDecimal pkAsistencia, BigInteger cantidadhoras, Date fechainicio, BigInteger horasasistidas) {
        this.pkAsistencia = pkAsistencia;
        this.cantidadhoras = cantidadhoras;
        this.fechainicio = fechainicio;
        this.horasasistidas = horasasistidas;
    }

    public BigDecimal getPkAsistencia() {
        return pkAsistencia;
    }

    public void setPkAsistencia(BigDecimal pkAsistencia) {
        this.pkAsistencia = pkAsistencia;
    }

    public BigInteger getCantidadhoras() {
        return cantidadhoras;
    }

    public void setCantidadhoras(BigInteger cantidadhoras) {
        this.cantidadhoras = cantidadhoras;
    }

    public Date getFechainicio() {
        return fechainicio;
    }

    public void setFechainicio(Date fechainicio) {
        this.fechainicio = fechainicio;
    }

    public BigInteger getHorasasistidas() {
        return horasasistidas;
    }

    public void setHorasasistidas(BigInteger horasasistidas) {
        this.horasasistidas = horasasistidas;
    }

    public Actividadludica getFkActividadludica() {
        return fkActividadludica;
    }

    public void setFkActividadludica(Actividadludica fkActividadludica) {
        this.fkActividadludica = fkActividadludica;
    }

    public Aprendiz getFkAprendiz() {
        return fkAprendiz;
    }

    public void setFkAprendiz(Aprendiz fkAprendiz) {
        this.fkAprendiz = fkAprendiz;
    }

    public Evento getFkEvento() {
        return fkEvento;
    }

    public void setFkEvento(Evento fkEvento) {
        this.fkEvento = fkEvento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkAsistencia != null ? pkAsistencia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Asistencia)) {
            return false;
        }
        Asistencia other = (Asistencia) object;
        if ((this.pkAsistencia == null && other.pkAsistencia != null) || (this.pkAsistencia != null && !this.pkAsistencia.equals(other.pkAsistencia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "timesoft2.jpa.entidades.Asistencia[ pkAsistencia=" + pkAsistencia + " ]";
    }
    
}
