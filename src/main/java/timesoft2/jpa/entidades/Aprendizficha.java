/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timesoft2.jpa.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PC
 */
@Entity
@Table(name = "APRENDIZFICHA", catalog = "", schema = "TIMES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Aprendizficha.findAll", query = "SELECT a FROM Aprendizficha a")
    , @NamedQuery(name = "Aprendizficha.findByPkAprendizficah", query = "SELECT a FROM Aprendizficha a WHERE a.pkAprendizficah = :pkAprendizficah")})
public class Aprendizficha implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PK_APRENDIZFICAH", nullable = false, precision = 0, scale = -127)
    private BigDecimal pkAprendizficah;
    @JoinColumn(name = "FK_APRENDIZ", referencedColumnName = "PK_APRENDIZ")
    @ManyToOne
    private Aprendiz fkAprendiz;
    @JoinColumn(name = "FK_FICHA", referencedColumnName = "PK_FICHA")
    @ManyToOne
    private Ficha fkFicha;

    public Aprendizficha() {
    }

    public Aprendizficha(BigDecimal pkAprendizficah) {
        this.pkAprendizficah = pkAprendizficah;
    }

    public BigDecimal getPkAprendizficah() {
        return pkAprendizficah;
    }

    public void setPkAprendizficah(BigDecimal pkAprendizficah) {
        this.pkAprendizficah = pkAprendizficah;
    }

    public Aprendiz getFkAprendiz() {
        return fkAprendiz;
    }

    public void setFkAprendiz(Aprendiz fkAprendiz) {
        this.fkAprendiz = fkAprendiz;
    }

    public Ficha getFkFicha() {
        return fkFicha;
    }

    public void setFkFicha(Ficha fkFicha) {
        this.fkFicha = fkFicha;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkAprendizficah != null ? pkAprendizficah.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Aprendizficha)) {
            return false;
        }
        Aprendizficha other = (Aprendizficha) object;
        if ((this.pkAprendizficah == null && other.pkAprendizficah != null) || (this.pkAprendizficah != null && !this.pkAprendizficah.equals(other.pkAprendizficah))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "timesoft2.jpa.entidades.Aprendizficha[ pkAprendizficah=" + pkAprendizficah + " ]";
    }
    
}
