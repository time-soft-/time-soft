/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timesoft2.jpa.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author PC
 */
@Entity
@Table(name = "EVENTO", catalog = "", schema = "TIMES", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"CODIGO"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Evento.findAll", query = "SELECT e FROM Evento e")
    , @NamedQuery(name = "Evento.findByPkEvento", query = "SELECT e FROM Evento e WHERE e.pkEvento = :pkEvento")
    , @NamedQuery(name = "Evento.findByCodigo", query = "SELECT e FROM Evento e WHERE e.codigo = :codigo")
    , @NamedQuery(name = "Evento.findByNombre", query = "SELECT e FROM Evento e WHERE e.nombre = :nombre")
    , @NamedQuery(name = "Evento.findByFechainiciEvento", query = "SELECT e FROM Evento e WHERE e.fechainiciEvento = :fechainiciEvento")
    , @NamedQuery(name = "Evento.findByFechafinal", query = "SELECT e FROM Evento e WHERE e.fechafinal = :fechafinal")
    , @NamedQuery(name = "Evento.findByCanthoras", query = "SELECT e FROM Evento e WHERE e.canthoras = :canthoras")})
public class Evento implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PK_EVENTO", nullable = false, precision = 0, scale = -127)
    private BigDecimal pkEvento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "CODIGO", nullable = false, length = 20)
    private String codigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "NOMBRE", nullable = false, length = 500)
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHAINICI_EVENTO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechainiciEvento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHAFINAL", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechafinal;
    @Column(name = "CANTHORAS")
    private BigInteger canthoras;
    @OneToMany(mappedBy = "fkEvento")
    private List<Estadoludica> estadoludicaList;
    @JoinColumn(name = "FK_TIPOEVENTO", referencedColumnName = "PK_TIPOEVENTO", nullable = false)
    @ManyToOne(optional = false)
    private Tipoevento fkTipoevento;
    @OneToMany(mappedBy = "fkEvento")
    private List<Asistencia> asistenciaList;

    public Evento() {
    }

    public Evento(BigDecimal pkEvento) {
        this.pkEvento = pkEvento;
    }

    public Evento(BigDecimal pkEvento, String codigo, String nombre, Date fechainiciEvento, Date fechafinal) {
        this.pkEvento = pkEvento;
        this.codigo = codigo;
        this.nombre = nombre;
        this.fechainiciEvento = fechainiciEvento;
        this.fechafinal = fechafinal;
    }

    public BigDecimal getPkEvento() {
        return pkEvento;
    }

    public void setPkEvento(BigDecimal pkEvento) {
        this.pkEvento = pkEvento;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechainiciEvento() {
        return fechainiciEvento;
    }

    public void setFechainiciEvento(Date fechainiciEvento) {
        this.fechainiciEvento = fechainiciEvento;
    }

    public Date getFechafinal() {
        return fechafinal;
    }

    public void setFechafinal(Date fechafinal) {
        this.fechafinal = fechafinal;
    }

    public BigInteger getCanthoras() {
        return canthoras;
    }

    public void setCanthoras(BigInteger canthoras) {
        this.canthoras = canthoras;
    }

    @XmlTransient
    public List<Estadoludica> getEstadoludicaList() {
        return estadoludicaList;
    }

    public void setEstadoludicaList(List<Estadoludica> estadoludicaList) {
        this.estadoludicaList = estadoludicaList;
    }

    public Tipoevento getFkTipoevento() {
        return fkTipoevento;
    }

    public void setFkTipoevento(Tipoevento fkTipoevento) {
        this.fkTipoevento = fkTipoevento;
    }

    @XmlTransient
    public List<Asistencia> getAsistenciaList() {
        return asistenciaList;
    }

    public void setAsistenciaList(List<Asistencia> asistenciaList) {
        this.asistenciaList = asistenciaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkEvento != null ? pkEvento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Evento)) {
            return false;
        }
        Evento other = (Evento) object;
        if ((this.pkEvento == null && other.pkEvento != null) || (this.pkEvento != null && !this.pkEvento.equals(other.pkEvento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "timesoft2.jpa.entidades.Evento[ pkEvento=" + pkEvento + " ]";
    }
    
}
