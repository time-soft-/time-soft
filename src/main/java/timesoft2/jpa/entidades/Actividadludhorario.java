/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timesoft2.jpa.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PC
 */
@Entity
@Table(name = "ACTIVIDADLUDHORARIO", catalog = "", schema = "TIMES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Actividadludhorario.findAll", query = "SELECT a FROM Actividadludhorario a")
    , @NamedQuery(name = "Actividadludhorario.findByPkActividadludhorario", query = "SELECT a FROM Actividadludhorario a WHERE a.pkActividadludhorario = :pkActividadludhorario")})
public class Actividadludhorario implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PK_ACTIVIDADLUDHORARIO", nullable = false, precision = 0, scale = -127)
    private BigDecimal pkActividadludhorario;
    @JoinColumn(name = "FK_ACTIVIDADLUDICA", referencedColumnName = "PK_ACTIVIDADLUDICA", nullable = false)
    @ManyToOne(optional = false)
    private Actividadludica fkActividadludica;
    @JoinColumn(name = "FK_HORARIO", referencedColumnName = "PK_HORARIO", nullable = false)
    @ManyToOne(optional = false)
    private Horario fkHorario;

    public Actividadludhorario() {
    }

    public Actividadludhorario(BigDecimal pkActividadludhorario) {
        this.pkActividadludhorario = pkActividadludhorario;
    }

    public BigDecimal getPkActividadludhorario() {
        return pkActividadludhorario;
    }

    public void setPkActividadludhorario(BigDecimal pkActividadludhorario) {
        this.pkActividadludhorario = pkActividadludhorario;
    }

    public Actividadludica getFkActividadludica() {
        return fkActividadludica;
    }

    public void setFkActividadludica(Actividadludica fkActividadludica) {
        this.fkActividadludica = fkActividadludica;
    }

    public Horario getFkHorario() {
        return fkHorario;
    }

    public void setFkHorario(Horario fkHorario) {
        this.fkHorario = fkHorario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkActividadludhorario != null ? pkActividadludhorario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Actividadludhorario)) {
            return false;
        }
        Actividadludhorario other = (Actividadludhorario) object;
        if ((this.pkActividadludhorario == null && other.pkActividadludhorario != null) || (this.pkActividadludhorario != null && !this.pkActividadludhorario.equals(other.pkActividadludhorario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "timesoft2.jpa.entidades.Actividadludhorario[ pkActividadludhorario=" + pkActividadludhorario + " ]";
    }
    
}
