/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timesoft2.jpa.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author PC
 */
@Entity
@Table(name = "APRENDIZ", catalog = "", schema = "TIMES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Aprendiz.findAll", query = "SELECT a FROM Aprendiz a")
    , @NamedQuery(name = "Aprendiz.findByPkAprendiz", query = "SELECT a FROM Aprendiz a WHERE a.pkAprendiz = :pkAprendiz")
    , @NamedQuery(name = "Aprendiz.findByNumeroidentificacion", query = "SELECT a FROM Aprendiz a WHERE a.numeroidentificacion = :numeroidentificacion")
    , @NamedQuery(name = "Aprendiz.findByNombre", query = "SELECT a FROM Aprendiz a WHERE a.nombre = :nombre")
    , @NamedQuery(name = "Aprendiz.findByEmail", query = "SELECT a FROM Aprendiz a WHERE a.email = :email")})
public class Aprendiz implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PK_APRENDIZ", nullable = false, precision = 0, scale = -127)
    private BigDecimal pkAprendiz;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUMEROIDENTIFICACION", nullable = false)
    private BigInteger numeroidentificacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE", nullable = false, length = 100)
    private String nombre;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "EMAIL", nullable = false, length = 50)
    private String email;
    @OneToMany(mappedBy = "fkAprendiz")
    private List<Aprendizficha> aprendizfichaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkAprendiz")
    private List<Asistencia> asistenciaList;
    @JoinColumn(name = "FK_PERSONA", referencedColumnName = "PK_PERSONA")
    @ManyToOne
    private Persona fkPersona;

    public Aprendiz() {
    }

    public Aprendiz(BigDecimal pkAprendiz) {
        this.pkAprendiz = pkAprendiz;
    }

    public Aprendiz(BigDecimal pkAprendiz, BigInteger numeroidentificacion, String nombre, String email) {
        this.pkAprendiz = pkAprendiz;
        this.numeroidentificacion = numeroidentificacion;
        this.nombre = nombre;
        this.email = email;
    }

    public BigDecimal getPkAprendiz() {
        return pkAprendiz;
    }

    public void setPkAprendiz(BigDecimal pkAprendiz) {
        this.pkAprendiz = pkAprendiz;
    }

    public BigInteger getNumeroidentificacion() {
        return numeroidentificacion;
    }

    public void setNumeroidentificacion(BigInteger numeroidentificacion) {
        this.numeroidentificacion = numeroidentificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @XmlTransient
    public List<Aprendizficha> getAprendizfichaList() {
        return aprendizfichaList;
    }

    public void setAprendizfichaList(List<Aprendizficha> aprendizfichaList) {
        this.aprendizfichaList = aprendizfichaList;
    }

    @XmlTransient
    public List<Asistencia> getAsistenciaList() {
        return asistenciaList;
    }

    public void setAsistenciaList(List<Asistencia> asistenciaList) {
        this.asistenciaList = asistenciaList;
    }

    public Persona getFkPersona() {
        return fkPersona;
    }

    public void setFkPersona(Persona fkPersona) {
        this.fkPersona = fkPersona;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkAprendiz != null ? pkAprendiz.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Aprendiz)) {
            return false;
        }
        Aprendiz other = (Aprendiz) object;
        if ((this.pkAprendiz == null && other.pkAprendiz != null) || (this.pkAprendiz != null && !this.pkAprendiz.equals(other.pkAprendiz))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "timesoft2.jpa.entidades.Aprendiz[ pkAprendiz=" + pkAprendiz + " ]";
    }
    
}
