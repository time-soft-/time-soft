/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timesoft2.ejb.fachadas;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import timesoft2.jpa.entidades.Aprendizficha;

/**
 *
 * @author PC
 */
@Stateless
public class AprendizfichaFacade extends AbstractFacade<Aprendizficha> {

    @PersistenceContext(unitName = "gov.sena_TimeSoft2_war_1.0.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AprendizfichaFacade() {
        super(Aprendizficha.class);
    }
    
}
