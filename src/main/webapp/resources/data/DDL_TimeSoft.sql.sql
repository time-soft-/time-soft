/*==============================================================*/
/* DBMS name:      ORACLE Version 11g                           */
/* Created on:     19/02/2019 2:27:43 p. m.                     */
/*==============================================================*/


alter table ACCIONPERMITIDA
   drop constraint FK_ACCIONPE_REFERENCE_ACCIONP;

alter table ACCIONPERMITIDAPLANTILLA
   drop constraint FK_ACCIONPE_REFERENCE_ACCIONPE;

alter table ACCIONPERMITIDAPLANTILLA
   drop constraint FK_ACCIONPE_REFERENCE_PLANTILL;

alter table ACCIONPERMITIDAPORUSUARIO
   drop constraint FK_ACCIONPE_REFERENCE_USUARIO;

alter table ACCIONPERMITIDAPORUSUARIO
   drop constraint FK_ACCIONPE_REFERENCE_ACCIO;

alter table ACTIVIDADLUDHORARIO
   drop constraint FK_ACTIVIDA_REFERENCE_HORARIO;

alter table ACTIVIDADLUDHORARIO
   drop constraint FK_ACTIVIDA_REFERENCE_ACTIVIDA;

alter table ACTIVIDADLUDICA
   drop constraint FK_ACTIVIDA_REFERENCE_LUDICA;

alter table ALINSTRUCTOR
   drop constraint FK_ALINSTRU_REFERENCE_ACTIVIDA;

alter table ALINSTRUCTOR
   drop constraint FK_ALINSTRU_REFERENCE_INSTRUCT;

alter table APRENDIZ
   drop constraint FK_APRENDIZ_REFERENCE_PERSONA;

alter table APRENDIZFICHA
   drop constraint FK_APRENDIZ_REFERENCE_APRENDIZ;

alter table APRENDIZFICHA
   drop constraint FK_APRENDIZ_REFERENCE_FICHA;

alter table ASISTENCIA
   drop constraint FK_ASISTENC_REFERENCE_EVENTO;

alter table ASISTENCIA
   drop constraint FK_ASISTENC_REFERENCE_APRENDIZ;

alter table ASISTENCIA
   drop constraint FK_ASISTENC_REFERENCE_ACTIVIDA;

alter table DEPARTAMENTO
   drop constraint FK_DEPARTAM_REFERENCE_PAIS;

alter table ESTADOLUDICA
   drop constraint FK_ESTADOLU_REFERENCE_LUDICA;

alter table ESTADOLUDICA
   drop constraint FK_ESTADOLU_REFERENCE_EVENTO;

alter table ESTADOLUDICA
   drop constraint FK_ESTADOLU_REFERENCE_ESTADO;

alter table ESTADOUSUARIO
   drop constraint FK_ESTADOUS_REFERENCE_ESTADO;

alter table ESTADOUSUARIO
   drop constraint FK_ESTADOUS_REFERENCE_USUARIO;

alter table EVENTO
   drop constraint FK_EVENTO_REFERENCE_TIPOEVEN;

alter table FICHA
   drop constraint FK_FICHA_REFERENCE_PROGRAMA;

alter table HORARIO
   drop constraint FK_HORARIO_REFERENCE_LUGAR;

alter table INSTRUCTOR
   drop constraint FK_INSTRUCT_REFERENCE_PERSONA;

alter table LUDICA
   drop constraint FK_LUDICA_REFERENCE_HORARIO;

alter table MUNICIPIO
   drop constraint FK_MUNICIPI_REFERENCE_DEPARTAM;

alter table PERSONA
   drop constraint FK_PERSONA_REFERENCE_MUNICIPI;

alter table PERSONA
   drop constraint FK_PERSONA_REFERENCE_TIPOIDEN;

alter table USUARIO
   drop constraint FK_USUARIO_REFERENCE_PERSONA;

drop index UNIQUE_CODIGO;

drop table ACCIONPERMITIDA cascade constraints;

drop table ACCIONPERMITIDAPLANTILLA cascade constraints;

drop table ACCIONPERMITIDAPORUSUARIO cascade constraints;

drop table ACTIVIDADLUDHORARIO cascade constraints;

drop table ACTIVIDADLUDICA cascade constraints;

drop table ALINSTRUCTOR cascade constraints;

drop table APRENDIZ cascade constraints;

drop table APRENDIZFICHA cascade constraints;

drop table ASISTENCIA cascade constraints;

drop index UNIQUE_CODIGODEPTO;

drop table DEPARTAMENTO cascade constraints;

drop index UNIQUE_CODIGOESTADO;

drop table ESTADO cascade constraints;

drop table ESTADOLUDICA cascade constraints;

drop table ESTADOUSUARIO cascade constraints;

drop index UNIQUE_CODIGOE;

drop table EVENTO cascade constraints;

drop index UNIQUE_CODIGOF;

drop table FICHA cascade constraints;

drop table HORARIO cascade constraints;

drop table INSTRUCTOR cascade constraints;

drop index UNIQUE_CODIGOLUDICA;

drop table LUDICA cascade constraints;

drop index UNIQUE_CODIGOL;

drop table LUGAR cascade constraints;

drop index UNIQUE_CODIGOMUNICIPIO;

drop table MUNICIPIO cascade constraints;

drop index UNIQUE_CODIGOPAIS;

drop table PAIS cascade constraints;

drop index UNIQUE_CORREO;

drop index UNIQUE_IDENTIFICACION;

drop table PERSONA cascade constraints;

drop index UNIQUE_CODIGOAP;

drop table PLANTILLAACCIONPERMITIDA cascade constraints;

drop index UNIQUE_CODIGOP;

drop table PROGRAMAFORMACION cascade constraints;

drop table TIPOEVENTO cascade constraints;

drop index UNIQUE_CODIGOTIDENTIFICACION;

drop table TIPOIDENTIFICACION cascade constraints;

drop index UNIQUE_NOMBREUSUARIO;

drop table USUARIO cascade constraints;

drop sequence SQACCIONPERMITIDA;

drop sequence SQACCIONPERMITIDAPLANTILLA;

drop sequence SQACCIONPORUSUARIO;

drop sequence SQACTIVIDADLUDHORARIO;

drop sequence SQACTIVIDADLUDICA;

drop sequence SQALINSTRUCTOR;

drop sequence SQAPRENDIZ;

drop sequence SQAPRENDIZFICHA;

drop sequence SQASISTENCIA;

drop sequence SQASISTENCIAFICHA;

drop sequence SQDEPARTAMENTO;

drop sequence SQESTADO;

drop sequence SQESTADOLUDICA;

drop sequence SQESTADOUSUARIO;

drop sequence SQEVENTO;

drop sequence SQFICHA;

drop sequence SQHORARIO;

drop sequence SQINSTRUCTOR;

drop sequence SQINSTRUCTORHORARIO;

drop sequence SQLUDICA;

drop sequence SQLUGAR;

drop sequence SQMUNICIPIO;

drop sequence SQPAIS;

drop sequence SQPERSONA;

drop sequence SQPLANTILLAACCIONPERMITIDA;

drop sequence SQPROGRAMAFORMACIO;

drop sequence SQTELEFONO;

drop sequence SQTIPOEVENTO;

drop sequence SQTIPOIDENTIFICACION;

drop sequence SQTIPOTELEFONO;

drop sequence SQUSUARIO;

create sequence SQACCIONPERMITIDA
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQACCIONPERMITIDAPLANTILLA
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQACCIONPORUSUARIO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQACTIVIDADLUDHORARIO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQACTIVIDADLUDICA
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQALINSTRUCTOR
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQAPRENDIZ
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQAPRENDIZFICHA
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQASISTENCIA
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQASISTENCIAFICHA
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQDEPARTAMENTO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQESTADO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQESTADOLUDICA
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQESTADOUSUARIO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQEVENTO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQFICHA;

create sequence SQHORARIO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQINSTRUCTOR
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQINSTRUCTORHORARIO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQLUDICA
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQLUGAR
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQMUNICIPIO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQPAIS
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQPERSONA
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQPLANTILLAACCIONPERMITIDA
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQPROGRAMAFORMACIO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQTELEFONO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQTIPOEVENTO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQTIPOIDENTIFICACION
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQTIPOTELEFONO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQUSUARIO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

/*==============================================================*/
/* Table: ACCIONPERMITIDA                                       */
/*==============================================================*/
create table ACCIONPERMITIDA 
(
   PK_ACCIONPERMITIDA   NUMBER               not null,
   FK_ACCIONPERMITIDA   NUMBER,
   CODIGO               VARCHAR2(20)         not null,
   DESCRIPCION          VARCHAR2(500)        not null,
   RUTA                 VARCHAR2(500)        not null,
   constraint PK_ACCIONPERMITIDA primary key (PK_ACCIONPERMITIDA)
);

/*==============================================================*/
/* Index: UNIQUE_CODIGO                                         */
/*==============================================================*/
create unique index UNIQUE_CODIGO on ACCIONPERMITIDA (
   CODIGO ASC
);

/*==============================================================*/
/* Table: ACCIONPERMITIDAPLANTILLA                              */
/*==============================================================*/
create table ACCIONPERMITIDAPLANTILLA 
(
   PK_ACCIONPERMITIDAPLANTILLA NUMBER               not null,
   FK_ACCIONPERMITIDA   NUMBER               not null,
   FK_PLANTILLAACCIONPERMITIDA INTEGER              not null,
   constraint PK_ACCIONPERMITIDAPLANTILLA primary key (PK_ACCIONPERMITIDAPLANTILLA)
);

/*==============================================================*/
/* Table: ACCIONPERMITIDAPORUSUARIO                             */
/*==============================================================*/
create table ACCIONPERMITIDAPORUSUARIO 
(
   PK_ACCPERUSU         NUMBER               not null,
   FK_USUARIO           NUMBER               not null,
   FK_ACCIONPERMITIDA   NUMBER               not null,
   constraint PK_ACCIONPERMITIDAPORUSUARIO primary key (PK_ACCPERUSU)
);

/*==============================================================*/
/* Table: ACTIVIDADLUDHORARIO                                   */
/*==============================================================*/
create table ACTIVIDADLUDHORARIO 
(
   PK_ACTIVIDADLUDHORARIO NUMBER               not null,
   FK_HORARIO           NUMBER               not null,
   FK_ACTIVIDADLUDICA   NUMBER               not null,
   constraint PK_ACTIVIDADLUDHORARIO primary key (PK_ACTIVIDADLUDHORARIO)
);

/*==============================================================*/
/* Table: ACTIVIDADLUDICA                                       */
/*==============================================================*/
create table ACTIVIDADLUDICA 
(
   PK_ACTIVIDADLUDICA   NUMBER               not null,
   FK_LUDICA            NUMBER,
   NOMBRE               VARCHAR2(200)        not null,
   constraint PK_ACTIVIDADLUDICA primary key (PK_ACTIVIDADLUDICA)
);

/*==============================================================*/
/* Table: ALINSTRUCTOR                                          */
/*==============================================================*/
create table ALINSTRUCTOR 
(
   PK_ALINSTRUCTOR      NUMBER               not null,
   FK_ACTIVIDADLUDICA   NUMBER               not null,
   FK_INSTRUCTOR        NUMBER               not null,
   constraint PK_ALINSTRUCTOR primary key (PK_ALINSTRUCTOR)
);

/*==============================================================*/
/* Table: APRENDIZ                                              */
/*==============================================================*/
create table APRENDIZ 
(
   PK_APRENDIZ          NUMBER               not null,
   FK_PERSONA           NUMBER,
   NUMEROIDENTIFICACION NUMBER               not null,
   NOMBRE               VARCHAR2(100)        not null,
   EMAIL                VARCHAR2(50)         not null,
   constraint PK_APRENDIZ primary key (PK_APRENDIZ)
);

/*==============================================================*/
/* Table: APRENDIZFICHA                                         */
/*==============================================================*/
create table APRENDIZFICHA 
(
   PK_APRENDIZFICAH     NUMBER               not null,
   FK_APRENDIZ          NUMBER(20),
   FK_FICHA             NUMBER(20),
   constraint PK_APRENDIZFICHA primary key (PK_APRENDIZFICAH)
);

/*==============================================================*/
/* Table: ASISTENCIA                                            */
/*==============================================================*/
create table ASISTENCIA 
(
   PK_ASISTENCIA        NUMBER               not null,
   FK_EVENTO            NUMBER,
   FK_APRENDIZ          NUMBER               not null,
   FK_ACTIVIDADLUDICA   NUMBER,
   CANTIDADHORAS        NUMBER               not null,
   FECHAINICIO          DATE                 not null,
   HORASASISTIDAS       NUMBER               not null,
   constraint PK_ASISTENCIA primary key (PK_ASISTENCIA)
);

/*==============================================================*/
/* Table: DEPARTAMENTO                                          */
/*==============================================================*/
create table DEPARTAMENTO 
(
   PK_DEPARTAMENTO      NUMBER               not null,
   FK_PAIS              NUMBER               not null,
   CODIGO               NVARCHAR2(20)        not null,
   NOMBRE               NVARCHAR2(100)       not null,
   constraint PK_DEPARTAMENTO primary key (PK_DEPARTAMENTO)
);

/*==============================================================*/
/* Index: UNIQUE_CODIGODEPTO                                    */
/*==============================================================*/
create unique index UNIQUE_CODIGODEPTO on DEPARTAMENTO (
   CODIGO ASC
);

/*==============================================================*/
/* Table: ESTADO                                                */
/*==============================================================*/
create table ESTADO 
(
   PK_ESTADO            NUMBER               not null,
   CODIGO               VARCHAR2(20)         not null,
   DESCRIPCION          VARCHAR2(100)        not null,
   SUSPENDIDO           VARCHAR2(20)         not null,
   constraint PK_ESTADO primary key (PK_ESTADO)
);

/*==============================================================*/
/* Index: UNIQUE_CODIGOESTADO                                   */
/*==============================================================*/
create unique index UNIQUE_CODIGOESTADO on ESTADO (
   CODIGO ASC
);

/*==============================================================*/
/* Table: ESTADOLUDICA                                          */
/*==============================================================*/
create table ESTADOLUDICA 
(
   PK_ESTODOLUDICA      NUMBER               not null,
   FK_LUDICA            NUMBER,
   FK_EVENTO            NUMBER,
   FK_ESTADO            NUMBER,
   FECHAMODIFICACION    DATE                 not null,
   constraint PK_ESTADOLUDICA primary key (PK_ESTODOLUDICA)
);

/*==============================================================*/
/* Table: ESTADOUSUARIO                                         */
/*==============================================================*/
create table ESTADOUSUARIO 
(
   PK_ESTADOUSUARIO     NUMBER               not null,
   FK_USUARIO           NUMBER               not null,
   FK_ESTADO            NUMBER               not null,
   FECHAINICIO          DATE                 not null,
   FECHAFIN             DATE,
   constraint PK_ESTADOUSUARIO primary key (PK_ESTADOUSUARIO)
);

/*==============================================================*/
/* Table: EVENTO                                                */
/*==============================================================*/
create table EVENTO 
(
   PK_EVENTO            NUMBER               not null,
   FK_TIPOEVENTO        NUMBER               not null,
   CODIGO               VARCHAR2(20)         not null,
   NOMBRE               VARCHAR(500)         not null,
   FECHAINICI_EVENTO    DATE                 not null,
   FECHAFINAL           DATE                 not null,
   CANTHORAS            NUMBER,
   constraint PK_EVENTO primary key (PK_EVENTO)
);

/*==============================================================*/
/* Index: UNIQUE_CODIGOE                                        */
/*==============================================================*/
create unique index UNIQUE_CODIGOE on EVENTO (
   CODIGO ASC
);

/*==============================================================*/
/* Table: FICHA                                                 */
/*==============================================================*/
create table FICHA 
(
   PK_FICHA             NUMBER               not null,
   FK_PROGRAMAFORMACION NUMBER,
   CODIGO               NUMBER               not null,
   constraint PK_FICHA primary key (PK_FICHA)
);

/*==============================================================*/
/* Index: UNIQUE_CODIGOF                                        */
/*==============================================================*/
create unique index UNIQUE_CODIGOF on FICHA (
   CODIGO ASC
);

/*==============================================================*/
/* Table: HORARIO                                               */
/*==============================================================*/
create table HORARIO 
(
   PK_HORARIO           NUMBER               not null,
   FK_LUGAR             NUMBER               not null,
   HORAINICIO           DATE                 not null,
   HORAFIN              DATE                 not null,
   DIA                  VARCHAR2(50)         not null,
   constraint PK_HORARIO primary key (PK_HORARIO)
);

/*==============================================================*/
/* Table: INSTRUCTOR                                            */
/*==============================================================*/
create table INSTRUCTOR 
(
   PK_INSTRUCTOR        NUMBER               not null,
   FK_PERSONA           NUMBER,
   NUMEROIDENTIFICACION NUMBER               not null,
   NOMBRE               VARCHAR2(200)        not null,
   constraint PK_INSTRUCTOR primary key (PK_INSTRUCTOR)
);

/*==============================================================*/
/* Table: LUDICA                                                */
/*==============================================================*/
create table LUDICA 
(
   PK_LUDICA            NUMBER               not null,
   FK_HORARIO           NUMBER,
   CODIGO               NUMBER               not null,
   NOMBRE               NUMBER               not null,
   constraint PK_LUDICA primary key (PK_LUDICA)
);

/*==============================================================*/
/* Index: UNIQUE_CODIGOLUDICA                                   */
/*==============================================================*/
create unique index UNIQUE_CODIGOLUDICA on LUDICA (
   CODIGO ASC
);

/*==============================================================*/
/* Table: LUGAR                                                 */
/*==============================================================*/
create table LUGAR 
(
   PK_LUGAR             NUMBER               not null,
   CODIGO               VARCHAR2(20)         not null,
   DESCRIPCION          VARCHAR2(500)        not null,
   constraint PK_LUGAR primary key (PK_LUGAR)
);

/*==============================================================*/
/* Index: UNIQUE_CODIGOL                                        */
/*==============================================================*/
create unique index UNIQUE_CODIGOL on LUGAR (
   CODIGO ASC
);

/*==============================================================*/
/* Table: MUNICIPIO                                             */
/*==============================================================*/
create table MUNICIPIO 
(
   PK_MUNICIPIO         NUMBER               not null,
   FK_DEPARTAMENTO      NUMBER,
   CODIGO               NVARCHAR2(20)        not null,
   NOMBRE               NVARCHAR2(100)       not null,
   constraint PK_MUNICIPIO primary key (PK_MUNICIPIO)
);

/*==============================================================*/
/* Index: UNIQUE_CODIGOMUNICIPIO                                */
/*==============================================================*/
create unique index UNIQUE_CODIGOMUNICIPIO on MUNICIPIO (
   CODIGO ASC
);

/*==============================================================*/
/* Table: PAIS                                                  */
/*==============================================================*/
create table PAIS 
(
   PK_PAIS              NUMBER               not null,
   CODIGO               NVARCHAR2(20)        not null,
   NOMBRE               NVARCHAR2(100)       not null,
   constraint PK_PAIS primary key (PK_PAIS)
);

/*==============================================================*/
/* Index: UNIQUE_CODIGOPAIS                                     */
/*==============================================================*/
create unique index UNIQUE_CODIGOPAIS on PAIS (
   CODIGO ASC
);

/*==============================================================*/
/* Table: PERSONA                                               */
/*==============================================================*/
create table PERSONA 
(
   PK_PERSONA           NUMBER               not null,
   FK_MUNICIPIOORIGEN   NUMBER               not null,
   FK_TIPOIDENTIFICACION NUMBER               not null,
   IDENTIFICACION       NVARCHAR2(20)        not null,
   NOMBRES              NVARCHAR2(500)       not null,
   APELLIDOS            VARCHAR2(500)        not null,
   DIRECCION            VARCHAR2(500)        not null,
   CORREO               VARCHAR2(100)        not null,
   constraint PK_PERSONA primary key (PK_PERSONA)
);

/*==============================================================*/
/* Index: UNIQUE_IDENTIFICACION                                 */
/*==============================================================*/
create unique index UNIQUE_IDENTIFICACION on PERSONA (
   IDENTIFICACION ASC
);

/*==============================================================*/
/* Index: UNIQUE_CORREO                                         */
/*==============================================================*/
create unique index UNIQUE_CORREO on PERSONA (
   CORREO ASC
);

/*==============================================================*/
/* Table: PLANTILLAACCIONPERMITIDA                              */
/*==============================================================*/
create table PLANTILLAACCIONPERMITIDA 
(
   PK_PLANTILLAACCIONPERMITIDA NUMBER               not null,
   CODIGO               VARCHAR2(20)         not null,
   DESCRIPCION          VARCHAR2(100)        not null,
   constraint PK_PLANTILLAACCIONPERMITIDA primary key (PK_PLANTILLAACCIONPERMITIDA)
);

/*==============================================================*/
/* Index: UNIQUE_CODIGOAP                                       */
/*==============================================================*/
create unique index UNIQUE_CODIGOAP on PLANTILLAACCIONPERMITIDA (
   CODIGO ASC
);

/*==============================================================*/
/* Table: PROGRAMAFORMACION                                     */
/*==============================================================*/
create table PROGRAMAFORMACION 
(
   PK_PROGRAMAFORMACION NUMBER               not null,
   NOMBRE               VARCHAR2(200)        not null,
   CODIGO               NUMBER               not null,
   constraint PK_PROGRAMAFORMACION primary key (PK_PROGRAMAFORMACION)
);

/*==============================================================*/
/* Index: UNIQUE_CODIGOP                                        */
/*==============================================================*/
create unique index UNIQUE_CODIGOP on PROGRAMAFORMACION (
   CODIGO ASC
);

/*==============================================================*/
/* Table: TIPOEVENTO                                            */
/*==============================================================*/
create table TIPOEVENTO 
(
   PK_TIPOEVENTO        NUMBER               not null,
   NOMBRE               VARCHAR(100)         not null,
   constraint PK_TIPOEVENTO primary key (PK_TIPOEVENTO)
);

/*==============================================================*/
/* Table: TIPOIDENTIFICACION                                    */
/*==============================================================*/
create table TIPOIDENTIFICACION 
(
   PK_TIPOIDENTIFICACION NUMBER               not null,
   CODIGO               VARCHAR2(20)         not null,
   DESCRIPCION          VARCHAR2(100)        not null,
   constraint PK_TIPOIDENTIFICACION primary key (PK_TIPOIDENTIFICACION)
);

/*==============================================================*/
/* Index: UNIQUE_CODIGOTIDENTIFICACION                          */
/*==============================================================*/
create unique index UNIQUE_CODIGOTIDENTIFICACION on TIPOIDENTIFICACION (
   CODIGO ASC
);

/*==============================================================*/
/* Table: USUARIO                                               */
/*==============================================================*/
create table USUARIO 
(
   PK_USUARIO           NUMBER               not null,
   FK_PERSONA           NUMBER               not null,
   NOMBREUSUARIO        VARCHAR2(200)        not null,
   CONTRASENA           VARCHAR2(200)        not null,
   TIPOUSUARIOENUM      SMALLINT             not null,
   constraint PK_USUARIO primary key (PK_USUARIO)
);

/*==============================================================*/
/* Index: UNIQUE_NOMBREUSUARIO                                  */
/*==============================================================*/
create unique index UNIQUE_NOMBREUSUARIO on USUARIO (
   NOMBREUSUARIO ASC
);

alter table ACCIONPERMITIDA
   add constraint FK_ACCIONPE_REFERENCE_ACCIONP foreign key (FK_ACCIONPERMITIDA)
      references ACCIONPERMITIDA (PK_ACCIONPERMITIDA);

alter table ACCIONPERMITIDAPLANTILLA
   add constraint FK_ACCIONPE_REFERENCE_ACCIONPE foreign key (FK_ACCIONPERMITIDA)
      references ACCIONPERMITIDA (PK_ACCIONPERMITIDA);

alter table ACCIONPERMITIDAPLANTILLA
   add constraint FK_ACCIONPE_REFERENCE_PLANTILL foreign key (FK_PLANTILLAACCIONPERMITIDA)
      references PLANTILLAACCIONPERMITIDA (PK_PLANTILLAACCIONPERMITIDA);

alter table ACCIONPERMITIDAPORUSUARIO
   add constraint FK_ACCIONPE_REFERENCE_USUARIO foreign key (FK_USUARIO)
      references USUARIO (PK_USUARIO);

alter table ACCIONPERMITIDAPORUSUARIO
   add constraint FK_ACCIONPE_REFERENCE_ACCIO foreign key (FK_ACCIONPERMITIDA)
      references ACCIONPERMITIDA (PK_ACCIONPERMITIDA);

alter table ACTIVIDADLUDHORARIO
   add constraint FK_ACTIVIDA_REFERENCE_HORARIO foreign key (FK_HORARIO)
      references HORARIO (PK_HORARIO);

alter table ACTIVIDADLUDHORARIO
   add constraint FK_ACTIVIDA_REFERENCE_ACTIVIDA foreign key (FK_ACTIVIDADLUDICA)
      references ACTIVIDADLUDICA (PK_ACTIVIDADLUDICA);

alter table ACTIVIDADLUDICA
   add constraint FK_ACTIVIDA_REFERENCE_LUDICA foreign key (FK_LUDICA)
      references LUDICA (PK_LUDICA);

alter table ALINSTRUCTOR
   add constraint FK_ALINSTRU_REFERENCE_ACTIVIDA foreign key (FK_ACTIVIDADLUDICA)
      references ACTIVIDADLUDICA (PK_ACTIVIDADLUDICA);

alter table ALINSTRUCTOR
   add constraint FK_ALINSTRU_REFERENCE_INSTRUCT foreign key (FK_INSTRUCTOR)
      references INSTRUCTOR (PK_INSTRUCTOR);

alter table APRENDIZ
   add constraint FK_APRENDIZ_REFERENCE_PERSONA foreign key (FK_PERSONA)
      references PERSONA (PK_PERSONA);

alter table APRENDIZFICHA
   add constraint FK_APRENDIZ_REFERENCE_APRENDIZ foreign key (FK_APRENDIZ)
      references APRENDIZ (PK_APRENDIZ);

alter table APRENDIZFICHA
   add constraint FK_APRENDIZ_REFERENCE_FICHA foreign key (FK_FICHA)
      references FICHA (PK_FICHA);

alter table ASISTENCIA
   add constraint FK_ASISTENC_REFERENCE_EVENTO foreign key (FK_EVENTO)
      references EVENTO (PK_EVENTO);

alter table ASISTENCIA
   add constraint FK_ASISTENC_REFERENCE_APRENDIZ foreign key (FK_APRENDIZ)
      references APRENDIZ (PK_APRENDIZ);

alter table ASISTENCIA
   add constraint FK_ASISTENC_REFERENCE_ACTIVIDA foreign key (FK_ACTIVIDADLUDICA)
      references ACTIVIDADLUDICA (PK_ACTIVIDADLUDICA);

alter table DEPARTAMENTO
   add constraint FK_DEPARTAM_REFERENCE_PAIS foreign key (FK_PAIS)
      references PAIS (PK_PAIS);

alter table ESTADOLUDICA
   add constraint FK_ESTADOLU_REFERENCE_LUDICA foreign key (FK_LUDICA)
      references LUDICA (PK_LUDICA);

alter table ESTADOLUDICA
   add constraint FK_ESTADOLU_REFERENCE_EVENTO foreign key (FK_EVENTO)
      references EVENTO (PK_EVENTO);

alter table ESTADOLUDICA
   add constraint FK_ESTADOLU_REFERENCE_ESTADO foreign key (FK_ESTADO)
      references ESTADO (PK_ESTADO);

alter table ESTADOUSUARIO
   add constraint FK_ESTADOUS_REFERENCE_ESTADO foreign key (FK_ESTADO)
      references ESTADO (PK_ESTADO);

alter table ESTADOUSUARIO
   add constraint FK_ESTADOUS_REFERENCE_USUARIO foreign key (FK_USUARIO)
      references USUARIO (PK_USUARIO);

alter table EVENTO
   add constraint FK_EVENTO_REFERENCE_TIPOEVEN foreign key (FK_TIPOEVENTO)
      references TIPOEVENTO (PK_TIPOEVENTO);

alter table FICHA
   add constraint FK_FICHA_REFERENCE_PROGRAMA foreign key (FK_PROGRAMAFORMACION)
      references PROGRAMAFORMACION (PK_PROGRAMAFORMACION);

alter table HORARIO
   add constraint FK_HORARIO_REFERENCE_LUGAR foreign key (FK_LUGAR)
      references LUGAR (PK_LUGAR);

alter table INSTRUCTOR
   add constraint FK_INSTRUCT_REFERENCE_PERSONA foreign key (FK_PERSONA)
      references PERSONA (PK_PERSONA);

alter table LUDICA
   add constraint FK_LUDICA_REFERENCE_HORARIO foreign key (FK_HORARIO)
      references HORARIO (PK_HORARIO);

alter table MUNICIPIO
   add constraint FK_MUNICIPI_REFERENCE_DEPARTAM foreign key (FK_DEPARTAMENTO)
      references DEPARTAMENTO (PK_DEPARTAMENTO);

alter table PERSONA
   add constraint FK_PERSONA_REFERENCE_MUNICIPI foreign key (FK_MUNICIPIOORIGEN)
      references MUNICIPIO (PK_MUNICIPIO);

alter table PERSONA
   add constraint FK_PERSONA_REFERENCE_TIPOIDEN foreign key (FK_TIPOIDENTIFICACION)
      references TIPOIDENTIFICACION (PK_TIPOIDENTIFICACION);

alter table USUARIO
   add constraint FK_USUARIO_REFERENCE_PERSONA foreign key (FK_PERSONA)
      references PERSONA (PK_PERSONA);

